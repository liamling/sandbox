#!/bin/bash

sudo apt update && sudo apt install unzip

uname -m | grep -q arm && ARCH=arm
uname -m | grep -q x86_64 && ARCH=x86_64

if [ ARCH == "x86_64" ]; then
    curl "https://awscli.amazonaws.com/awscli-exe-linux-${ARCH}.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
else
    # currently, there is not AWS CLI 2 Arm 32 version (64 version is available)
    curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
    unzip awscli-bundle.zip
    sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
fi