#!/bin/bash

sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
echo $'###################################################################
# Set system swappiness
# Set system vfs cache pressure
vm.swappiness=10
vm.vfs_cache_pressure=50' | sudo tee -a /etc/sysctl.conf