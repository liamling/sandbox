#!/bin/bash

# Script to read one or all temperature zones on a BeagleBone AI.
#
#  To see all temperature zones type ai_temp.sh from the command line.
#  To see a specific zone temperatures type ai_temp.sh X where X is the zone number
#   from 0 - 4. If you type in an invalid zone number you will get an error message.
#
#  This script does use bash specific commands, specifically the ones used to extract
#   characters from the temperature returned by the cat command. It also make a
#   couple of assumptions about the number of digits in the temperature. The first
#   is that there are no more that two digits to the left of the decimal point. This
#   is probably a safe assumption since the script will probably not run if the AI
#   temperature is > 99 deg. C. The second is that there there are two digits to the
#   left of the decimal point. This is a reasonable assumption because the AI is not
#   going below 10 deg. C unless some typesudo of extreme cooling is used. 
#
# This script is released to the public domain and is free to use 

thermal_zone=$1
declare -A zone_names=( ["0"]="CPU packge" ["1"]="GPU (SGX544 3D)" ["2"]="CORES (Arm Cortex-A15)" ["3"]="DSPEVE (between C66x and EVE)" ["4"]="IVA (IVA HD video engine)")

while :
do
  if [ -z $thermal_zone ]
  then

    # No command line zone number, show all zone temperatures

    zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

    # Change temperature format from xxxxx to xx.x
    # Extract temperature digits from the left and right of the decimal point

    ldp_temp=${zone_temp:0:2}
    rdp_temp=${zone_temp:2:1}

    # Recombine temperatue digits with decimal point added

    zone_temp="$ldp_temp.$rdp_temp"

    echo -e "zone 0 ${zone_names[0]} temperature = $zone_temp C"

    zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone1/temp`
    ldp_temp=${zone_temp:0:2}
    rdp_temp=${zone_temp:2:1}
    zone_temp="$ldp_temp.$rdp_temp"
    echo -e "zone 1 ${zone_names[1]} temperature = $zone_temp C"

    zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone2/temp`
    ldp_temp=${zone_temp:0:2}
    rdp_temp=${zone_temp:2:1}
    zone_temp="$ldp_temp.$rdp_temp"
    echo -e "zone 2 ${zone_names[2]} temperature = $zone_temp C"

    zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone3/temp`
    ldp_temp=${zone_temp:0:2}
    rdp_temp=${zone_temp:2:1}
    zone_temp="$ldp_temp.$rdp_temp"
    echo -e "zone 3 ${zone_names[3]} temperature = $zone_temp C"

    zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone4/temp`
    ldp_temp=${zone_temp:0:2}
    rdp_temp=${zone_temp:2:1}
    zone_temp="$ldp_temp.$rdp_temp"
    echo -e "zone 4 ${zone_names[4]} temperature = $zone_temp C"
    sleep 1
    tput cuu 5
    tput el
  else

    # Show zone temperature from command line zone number

    valid_zone="1"

    case $thermal_zone in
      "0")
        zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
        ;;
      "1")
        zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone1/temp`
        ;;
      "2")
        zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone2/temp`
        ;;
      "3")
        zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone3/temp`
        ;;
      "4")
        zone_temp=`cat /sys/devices/virtual/thermal/thermal_zone4/temp`
        ;;
      *)
        echo -e "Invalid temperature zone, valid temperature zones are 0 - 4"
        valid_zone="0"
        ;;
    esac

    if [ $valid_zone = "1" ]
      then
      ldp_temp=${zone_temp:0:2}
      rdp_temp=${zone_temp:2:1}
      zone_temp="$ldp_temp.$rdp_temp"
      echo -e "zone $thermal_zone ${zone_names[$thermal_zone]} temperature = $zone_temp C"
      sleep 1
      tput cuu1
      tput el
    fi
  fi
done
