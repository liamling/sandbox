#!/bin/bash
# ask for distribution
echo "Please specify the system distribution (debian/ubuntu): "
read DISTRO
# ask for architecture
echo "Please specify the system architecture (amd64/armhf/arm64): "
read ARCH

# install docker-compose
if ! type "docker-compose" &> /dev/null; then
    if [ ARCH == "amd64" ]; then
        sudo curl -L "https://github.com/docker/compose/releases/download/1.28.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
        type "docker-compose" &> /dev/null && echo docker-compose installed successfully!
    else
        sudo pip3 install wheel
        sudo pip3 install docker-compose
    fi
else
    echo docker-compose already installed!
fi

# install docker
if ! type "docker" &> /dev/null; then
    sudo apt-get remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
    curl -fsSL https://download.docker.com/linux/${DISTRO}/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
        "deb [arch=${ARCH} signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/${DISTRO} \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io
type "docker" &> /dev/null && echo docker installed successfully!
else
    echo docker already installed!
fi

# post-installation steps
sudo groupadd docker
sudo usermod -aG docker ${USER}
newgrp docker
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R
sudo systemctl enable docker.service
sudo systemctl enable containerd.service